package com.artivisi.training.devops201903.springbootbukutamu.controller;

import com.artivisi.training.devops201903.springbootbukutamu.dao.BukutamuDao;
import com.artivisi.training.devops201903.springbootbukutamu.entity.Bukutamu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BukutamuController {

    @Autowired private BukutamuDao bukutamuDao;

    @GetMapping("/api/bukutamu/")
    public Page<Bukutamu> tampilkanData(Pageable pageable) {
        return bukutamuDao.findAll(pageable);
    }
}
