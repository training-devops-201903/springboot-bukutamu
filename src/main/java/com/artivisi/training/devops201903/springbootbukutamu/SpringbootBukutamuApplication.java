package com.artivisi.training.devops201903.springbootbukutamu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootBukutamuApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBukutamuApplication.class, args);
	}

}
